from collections import defaultdict
import sqlite3 as dbapi

JOIN_TEMPLATE = ' join {table} on {from_table}.{from}={table}.{to}'


class Metadata:

    """Create select/insert/update templates from sqlite metadata."""

    table_query = "select * from sqlite_master where type='table'"
    table_info_query = "pragma table_info({})"
    index_query = "pragma index_list({})"
    index_info_query = "pragma index_info({})"
    foreign_key_query = "pragma foreign_key_list({})"

    def __init__(self, db):
        self.db = db
        tables = self._tables()
        self.primary_keys = {}
        self.non_primary_key_columns = {}
        self.unique_columns = {}
        self.foreign_keys = {}
        self.select_all_templates = {}
        self.select_one_templates = {}
        self.insert_templates = {}
        self.referenced_keys = {}
        self.select_by_id_templates = {}
        self.update_templates = {}
        self.table_dependencies = defaultdict(set)
        for table in tables:
            pk, others = self._columns(table)
            self.primary_keys[table] = pk
            self.non_primary_key_columns[table] = others
            self.unique_columns[table] = self._unique_columns(table)
            self.foreign_keys[table] = self._foreign_keys(table)
            self.select_all_templates[table] = self._select_all_template(table)
            self.insert_templates[table] = self._insert_template(table)
            self.select_one_templates[table] = self._select_one_template(table)
            self.update_templates[table] = self._update_template(table)
            self._find_table_dependencies(table)
        self.tables = self._sort_tables(tables)
        self._find_referenced_keys()
        self._create_select_by_id_templates()
        # self._sort_tables()

    def foreign_key_ids(self, table, row_ids):
        fk_ids = {}
        if row_ids:
            for fk_data in self.foreign_keys[table]:
                foreign_table = fk_data['table']
                if foreign_table in row_ids:
                    fk_ids[fk_data['from']] = row_ids[foreign_table]
        return fk_ids

    def _tables(self):
        cursor = self.db.exec_sql(self.table_query)
        return [row['name'] for row in cursor]

    def _columns(self, table):
        cursor = self.db.exec_sql(self.table_info_query.format(table))
        rows = cursor.fetchall()
        print('_columns:', table, [dict(r) for r in rows])
        # We assume one primary key column.
        primary_key = [row['name'] for row in rows if row['pk']][0]
        others = [row['name'] for row in rows if not row['pk']]
        return primary_key, others

    def _unique_columns(self, table):
        cursor = self.db.exec_sql(self.index_query.format(table))
        unique_indexes = [row['name'] for row in cursor if row['unique']]
        unique_cols = []
        for index in unique_indexes:
            cursor.execute(self.index_info_query.format(index))
            unique_cols.extend([row['name'] for row in cursor])
        return set(unique_cols)

    def _foreign_keys(self, table):
        cursor = self.db.exec_sql(self.foreign_key_query.format(table))
        return cursor.fetchall()

    def _insert_template(self, table):
        columns = self.non_primary_key_columns[table]
        column_string = ', '.join(columns)
        values_string = ', '.join(':{}'.format(col) for col in columns)
        return 'insert into {} ({}) values ({})'.format(table,
                                                        column_string,
                                                        values_string)


    def _unique_foreign_key_data(self, table):
        key_data = []
        unique_cols = set(self.unique_columns[table])
        for fk_data in self.foreign_keys[table]:
            if fk_data['from'] in unique_cols:
                data = {'from_table': table}
                data.update(fk_data)
                key_data.append(data)
        return key_data

    def _join_clause(self, table):
        return ' '.join(JOIN_TEMPLATE.format_map(data)
                        for data in self._unique_foreign_key_data(table))

    def _where_all_clause(self, table):
        clauses = []
        for data in self._unique_foreign_key_data(table):
            foreign_table = data['table']
            for col in self.unique_columns[foreign_table]:
                clause = '{}.{}=:{}'.format(foreign_table, col, col)
                clauses.append(clause)
        conditional = ' and '.join(clauses)
        return ' where {}'.format(conditional) if conditional else ''

    def _select_all_template(self, table):
        join_clause = self._join_clause(table)
        where_clause = self._where_all_clause(table)
        return 'select * from {}{}{}'.format(table, join_clause, where_clause)

    def _select_one_template(self, table):
        conditions = ['{}=:{}'.format(col, col)
                      for col in self.unique_columns[table]]
        condition = ' and '.join(conditions)
        return 'select * from {} where {}'.format(table, condition)

    def _update_template(self, table):
        pk = self.primary_keys[table]
        columns = self.non_primary_key_columns[table]
        set_clause = ', '.join('{}=:{}'.format(col, col) for col in columns)
        return 'update {} set {} where {}=:{}'.format(table,
                                                      set_clause,
                                                      pk,
                                                      pk)

    def _sort_tables(self, tables):
        sorted_tables = []
        while tables:
            table = tables.pop(0)
            dependencies = self.table_dependencies[table]
            if dependencies.issubset(sorted_tables):
                sorted_tables.append(table)
            else:
                tables.append(table)
        return sorted_tables

    def _find_referenced_keys(self):
        for table in self.tables:
            for fk_data in self.foreign_keys[table]:
                foreign_table = fk_data['table']
                ref_keys = self.referenced_keys.setdefault(foreign_table, set())
                ref_keys.add(fk_data['from'])

    def _create_select_by_id_templates(self):
        for table in self.tables:
            if table not in self.referenced_keys:
                continue
            refs = self.referenced_keys[table]
            pk = self.primary_keys[table]
            for ref in refs:
                conditions = ['{}=:{}'.format(pk, ref)]
            condition = ' or '.join(conditions)
            template = 'select * from {} where {}'.format(table, condition)
            self.select_by_id_templates[table] = template

    def _find_table_dependencies(self, table):
        dependencies = self.table_dependencies[table]
        if not dependencies:
            for fk_data in self.foreign_keys[table]:
                dependencies.add(fk_data['table'])
                dependencies.update(
                    self._find_table_dependencies(fk_data['table']))
        return dependencies




class Database:

    def __init__(self, filename):
        self.insert_sql = {}
        self.connection = dbapi.connect(filename)
        self.connection.row_factory = dbapi.Row
        self.metadata = Metadata(self)

    def exec_sql(self, sql, data=()):
        print('exec_sql', sql)
        cursor = self.connection.cursor()
        cursor.execute(sql, data)
        return cursor

    def fetch_all(self, table, data):
        cursor = self.exec_sql(self.metadata.select_all_templates[table], data)
        return cursor.fetchall()

    def fetch_one(self, table, data):
        cursor = self.exec_sql(self.metadata.select_one_templates[table], data)
        return cursor.fetchone()

    def fetch_by_id(self, table, data):
        cursor = self.exec_sql(self.metadata.select_by_id_templates[table],
                               data)
        return cursor.fetchone()

    def insert(self, table, data):
        cursor = self.exec_sql(self.metadata.insert_templates[table], data)
        return cursor.lastrowid

    def update(self, table, old, new):
        data = old.copy()
        row = self.fetch_one(table, old)
        data.update(row)  # populate with pk
        data.update(new)  # roll in new values
        tables = self.metadata.table_dependencies[table]
        data = self.save(data, tables)  # populate with any new foreign keys
        self.exec_sql(self.metadata.update_templates[table], data)

    def insert_list(self, table, data):
        cursor = self.connection.cursor()
        cursor.executemany(self.metadata.insert_templates[table], data)
        return cursor.rowcount

    def add(self, table, data):
        row = self.fetch_one(table, data)
        if row:
            pk = self.metadata.primary_keys[table]
            row_id = row[pk]
        else:
            try:
                row_id = self.insert(table, data)
            except dbapi.IntegrityError:
                row_id = None
        return row_id

    def add_and_update_ref(self, table, data):
        """Select or insert from data into table and update data
        with foreign key ids.  Return any missing keys from data."""
        unique_columns = self.metadata.unique_columns[table]
        # missing = unique_columns.difference(data)
        not_null = all(data.get(col) is not None for col in unique_columns)
        print(data, unique_columns)
        if not_null:
            row_id = self.add(table, data)
        else:
            row_id = None
        for ref in self.metadata.referenced_keys.get(table, []):
            data[ref] = row_id
        # return missing

    def save(self, data, tables=()):
        # Replace empty string with None
        input_data = {key: data[key] if data.get(key) else None for key in data}
        if not tables:
            tables = self.metadata.tables
        for table in tables:
            self.add_and_update_ref(table, input_data)
        # self.connection.commit()
        return input_data

    def commit(self):
        self.connection.commit()

    def sorted_rows(self, rows, sort_cols):
        col1, col2 = sort_cols
        def sort_key(row):
            key = row[col2]
            if key:
                return key
            return row[col1]
        return sorted(rows, key=sort_key)


def main():
    db = Database('tags.db')
    print(db.metadata.tables)
    print(db.metadata.select_all_templates)
    print(db.metadata.select_one_templates)
    print(db.metadata.insert_templates)
    print(db.metadata.select_by_id_templates)
    print(db.metadata.table_dependencies)
    db.save({'genre': None})
    # db.add('genre', {'genre': 'Classical'})
    data = dict(composer='Shostakovich',
                work='Symphony No. 5',
                worksort='Symphony No. 05',
                artist='Previn',
                artistsort='',
                conductor='Previn',
                band='LSO',
                genre='Symphony')
    output = db.save(data)
    print(output)
    works = db.fetch_all('work', data)
    print(dict(works[1]))
    genre = db.fetch_by_id('genre', dict(works[1]))
    print(genre)
    # print(db.fetch_one('genre', dict(works[0])))

if __name__ == '__main__':
    main()
