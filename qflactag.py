#!/usr/bin/env python

import sys
import os
import re
import shelve
from math import modf

# Python 2/3 compatibility
if not hasattr(__builtins__, 'unicode'):
    unicode = QString = str

from PyQt4.QtCore import *
from PyQt4.QtGui import *

Slot = pyqtSlot

from mutagen.flac import FLAC

from roman import toRoman

# must be generated from ui file with pyuic
from ui_qflactag import Ui_MainWindow

__version__ = "0.0.1"

# ROOT = '/media/music/sb_library/Music'
ROOT = '/run/user/1000/gvfs/smb-share:server=amd2,share=music/sb_library/Music'

TAGS = ('album', 'albumsort', 'title', 'composer', 'work', 'worksort', 'artist',
        'artistsort', 'conductor', 'band', 'genre', 'comment', 'uniquename',
        'tracknumber', 'albumartist')

WORKTAGS = {'album', 'albumsort', 'composer', 'work', 'worksort', 'artist',
                'artistsort', 'conductor', 'band', 'genre', 'uniquename',
                'albumartist'}

NECESSARY_TAGS = ('composer', 'work', 'artist', 'conductor', 'band')

COMPLETED_TAGS = ('composer', 'artist', 'conductor', 'band', 'work')

TAG_PREFIX = 'tagForm'

GENRES = ('Classical', 'Symphony', 'Chamber Music',
          'Opera', 'Sonata', 'Baroque', 'Piano', 'String Quartet')

SHELF = os.path.join(ROOT, 'flactags.db')

FLACTITLES = os.path.expanduser("~/.flactitles")
EDITOR = "emacs"

regex = re.compile('^([0-9]+ *[^a-zA-Z0-9)(]* *)*(.*)\.flac$')

comment_re = re.compile('^(.*): +([:alpha:]+)=(.*)$')
number_re = re.compile('(No. )([0-9]+)')


def gettag(audio, key, default=''):
    return audio.get(key.lower(), [default])[0]


def tagdata(lineEdit):
    name = lineEdit.objectName()
    tag = name[len(TAG_PREFIX):].lower()
    text = unicode(lineEdit.text())
    return tag, text


def flacpath(path):
    return path.endswith(".flac") and os.path.isfile(path)


def singleshot(func):
    def wrapper(self):
        def closure():
            return func(self)

        QTimer.singleShot(0, closure)

    return wrapper


def restore_settings(**kwargs):
    settings = QSettings()
    for key, setter in kwargs.items():
        value = settings.value(key)
        if value:
            setter(value)


class CompletionModel(QAbstractListModel):
    def __init__(self, aset, parent=None, sortkey=None):
        super(CompletionModel, self).__init__(parent)
        self.set = aset
        self.list = sorted(self.set)

    def rowCount(self, index=QModelIndex()):
        return len(self.list)

    def columnCount(self, *args):
        return 1

    def data(self, index, role=Qt.DisplayRole):
        if (index.isValid() and 0 <= index.row() < len(self.list) and
                role == Qt.DisplayRole or role == Qt.EditRole):
            return QString(self.list[index.row()])

    def refresh(self):
        """Call this whenever the backing set changes."""
        self.list = sorted(self.set)
        self.reset()


class Tagger(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(Tagger, self).__init__(parent)
        self.setupUi(self)

        model = QDirModel(self)
        self.dirTree.setModel(model)
        root = model.index(ROOT)
        self.dirTree.setRootIndex(root)
        self.dirTree.setColumnWidth(0, 800)
        # add completions to the genre input
        completer = QCompleter(GENRES, self)
        completer.setCompletionMode(QCompleter.InlineCompletion)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.tagFormGenre.setCompleter(completer)
        self.tagFormGenre.setText(GENRES[0])
        self.tagFormComposer.editingFinished.connect(
            self.on_composer_editingFinished)
        self.tagFormWork.editingFinished.connect(self.on_work_editingFinished)
        self.tableWidget.itemChanged.connect(self.tableItemChanged)
        # create a dict of tag editors
        self.lineEditMap = {}
        self.tagMap = {}

        def editingFinished(line_edit):
            def callback():
                self.on_editingFinished(line_edit)
            return callback

        # monkey patch a widget's focusOutEvent() method
        def wrapFocusOutEvent(widget, callback):
            oldFocusOutEvent = widget.focusOutEvent
            def newFocusOutEvent(*args):
                oldFocusOutEvent(*args)
                callback()
            widget.focusOutEvent = newFocusOutEvent

        for attr in dir(self):
            if attr.startswith(TAG_PREFIX):
                tag = attr[len(TAG_PREFIX):].lower()
                lineEdit = getattr(self, attr)
                self.lineEditMap[tag] = lineEdit
                self.tagMap[lineEdit] = tag
                handler = editingFinished(lineEdit)
                try:
                    lineEdit.editingFinished.connect(handler)
                except AttributeError:
                    wrapFocusOutEvent(lineEdit, handler)

        self.tagSets = {}
        self.workMap = self.tagSets['composer'] = {}  # self.workMap
        self.tagSets.update((tag, set()) for tag in COMPLETED_TAGS[1:])
        self._restoreSettings()
        for tag, tagset in self.tagSets.items():
            completer = QCompleter(CompletionModel(tagset, self), self)
            le = self.lineEditMap[tag]
            le.setCompleter(completer)

        QTimer.singleShot(0, self._sync_with_shelf)

    def closeEvent(self, event):
        self._saveSettings()

    @Slot()
    def on_action_Quit_triggered(self):
        self.close()

    @Slot()
    def on_actionSave_triggered(self):
        obj = self.focusWidget()

        def save():
            self._save(obj, action=True)

        QTimer.singleShot(0, save)

    @Slot()
    def on_actionSave_All_triggered(self):
        data = self._getData(WORKTAGS)

        def save():
            self._saveToSelected(data)

        QTimer.singleShot(0, save)

    @Slot()
    def on_actionPaste_Album_triggered(self):
        self._pasteToFocused(self.tagFormAlbum)

    @Slot()
    def on_actionPaste_Title_triggered(self):
        self._pasteToFocused(self.tagFormTitle)

    @Slot()
    def on_actionPaste_Artist_triggered(self):
        self._pasteToFocused(self.tagFormArtist)

    @Slot()
    def on_actionPaste_filename_triggered(self):
        for flac in self._selectedFlacs():
            # remove leading numerals
            # remove .flac suffix and replace underscores with spaces
            match = regex.match(os.path.basename(flac.filename))
            if match:
                name = match.group(2).replace('_', ' ')
                flac['work'] = flac['title'] = name
                flac.save()

    @Slot()
    def on_actionAdd_Album_triggered(self):
        pass

    @Slot()
    def on_actionAdd_Unique_Name_triggered(self):
        self.tagFormUniqueName.setText(self._uniqueName())
        self.tagFormUniqueName.setFocus()

    def _uniqueName(self, flac=None):
        if flac:
            def func(tag):
                return gettag(flac, tag)
        else:
            func = self._getText
        artist = func('artist')
        conductor = func('conductor')
        band = func('band')
        if artist:
            if conductor:
                return '%s/%s' % (artist, conductor)
            if band:
                return '%s/%s' % (artist, band)
            return artist
        if conductor:
            if band:
                return '%s/%s' % (conductor, band)
            return conductor
        return ''

    @Slot()
    def on_actionRefresh_File_Browser_triggered(self):
        self.dirTree.model().refresh()

    @Slot()
    def on_actionAdd_Work_Sort_triggered(self):
        for flac in self._selectedFlacs():
            work = flac.get('work', [''])[0]
            worksort = self._worksort(work)
            if worksort == work:
                if 'worksort' not in flac:
                    continue
                worksort = ''
            flac['worksort'] = worksort
            flac.save()

    def _worksort(self, work):
        parts = number_re.split(work)
        try:
            parts[2] = "%02d" % int(parts[2])
        except IndexError:
            pass
        return ''.join(parts)

    @singleshot
    @Slot()
    def on_action_Copy_Title_to_Work_triggered(self):
        for flac in self._selectedFlacs():
            title = flac.get('title', [''])[0]
            flac['work'] = title
            flac.save()

    @Slot()
    def on_action_Sequence_tracks_triggered(self):
        flacs = self._selectedFlacs()
        tracktotal = "%02d" % len(flacs)
        for i, flac in enumerate(flacs):
            flac['tracknumber'] = "%02d" % (i + 1)
            flac['tracktotal'] = tracktotal
            flac.save()

    @Slot()
    def on_actionAdd_Titles_triggered(self, withComposer=False):
        """Add titles to selected flacs from WORK tags."""
        for i, flac in enumerate(self._selectedFlacs()):
            work = flac.get('work', ['Work'])[0]
            if withComposer:
                composer = flac.get('composer', ['Composer'])[0]
                work = "%s: %s" % (composer, work)
            title = flac.get('title', [''])[0]
            flac['title'] = '%s: %s. %s' % (
                work, toRoman(i + 1), title)
            flac.save()

    @Slot()
    def on_actionAdd_Composer_and_Titles_triggered(self):
        """Add titles to selected flacs from COMPOSER and WORK tags."""
        self.on_actionAdd_Titles_triggered(True)

    @Slot()
    def on_actionRefresh_Form_Data_triggered(self):
        self._sync_with_shelf()

    @Slot()
    def on_actionRescan_triggered(self):
        i = 0
        message = 'Scanning Library'
        self.statusBar().showMessage('Scanning Library')
        shelf = shelve.open(SHELF)
        shelf.clear()
        for root, dirs, files in os.walk(ROOT):
            for f in files:
                if f.endswith('.flac'):
                    name = os.path.join(root, f)
                    flac = FLAC(name)
                    data = dict((key, flac[key][0]) for key in flac)
                    data['filename'] = name
                    shelf[flac.filename] = data
                    if i % 250 == 0:
                        message += '.'
                        self.statusBar().showMessage(message)
                    QApplication.processEvents()
                    i += 1
        self._sync_with_shelf(shelf)

    @Slot()
    def on_action_Edit_Titles_triggered(self):
        titlefile = open(FLACTITLES, "w")
        text = '\n'.join(gettag(flac, 'title', 'No Title')
                         for flac in self._selectedFlacs())
        encoding = os.sys.getfilesystemencoding()
        titlefile.write(text.encode(encoding))
        titlefile.close()
        os.system("%s %s" % (EDITOR, FLACTITLES))
        titlefile = open(FLACTITLES, "r")
        for line, flac in zip(titlefile.readlines(), self._selectedFlacs()):
            flac['title'] = line.rstrip().decode(encoding)
            flac.save()
        titlefile.close()

    def on_composer_editingFinished(self):
        self._setWorksCompleter()

    def on_work_editingFinished(self):
        composer = unicode(self.tagFormComposer.text())
        work = unicode(self.tagFormWork.text())
        if composer in self.workMap and work in self.workMap[composer]:
            workData = self.workMap[composer][work]
            worksort = workData.get('worksort', '')
            genre = workData.get('genre', '')
            self.tagFormWorkSort.setText(worksort)
            self.tagFormGenre.setText(genre)

    def on_editingFinished(self, lineEdit):
        self._save(lineEdit)

    def _getText(self, tag):
        return unicode(self.lineEditMap[tag.lower()].text())

    def _save(self, obj, action=False):
        if obj in self.tagMap:
            tag = self.tagMap[obj]
            tags = [tag]
            if tag == 'work':
                tags.extend(('worksort', 'genre'))
            data = self._getData(tags)
            self._saveToSelected(data, action)

    def _setWorksCompleter(self):
        composer = unicode(self.tagFormComposer.text())
        if not composer:
            return
        workset = self.tagSets['work']
        workset.clear()
        if composer in self.workMap:
            workset.update(self.workMap[composer])
        self.tagFormWork.completer().model().refresh()

    def _firstartist(self, bag, saveto=()):
        firstartist = None
        for tag in 'artist', 'conductor', 'band', 'uniquename':
            val = bag.get(tag)
            if val:
                if tag in saveto:
                    saveto[tag].add(val)
                if not firstartist:
                    firstartist = val
        return firstartist

    def _updateSets(self, bag):
        composer = bag.get('composer')
        if not composer:
            return
        work = bag.get('work')
        if not work:
            return
        genre = bag.get('genre')
        if genre not in GENRES:
            return
        firstartist = self._firstartist(bag, saveto=self.tagSets)
        if not firstartist:
            return
        self.workMap.setdefault(composer, {})[work] = bag

    def _sync_with_shelf(self, shelf=None):
        if not shelf:
            shelf = shelve.open(SHELF)
        # create local copies of our shelved tag data
        self.statusBar().showMessage('Synching Form Data')
        for tagset in self.tagSets.values():
            tagset.clear()
        items = shelf.items()
        for key, bag in items:
            self._updateSets(bag)
            QApplication.processEvents()

        def populateWorks():
            self._populateWorks(items)
            shelf.close()

        QTimer.singleShot(0, self._loadCompleters)
        QTimer.singleShot(0, populateWorks)

    def _loadCompleters(self):
        for tag in self.tagSets:
            lineEdit = self.lineEditMap[tag]
            lineEdit.completer().model().refresh()
        self.statusBar().showMessage('Ready')

    def _pasteToFocused(self, origin):
        target = self.focusWidget()
        if target in self.tagMap:
            origin.selectAll()
            origin.copy()
            target.paste()

    def _restoreSettings(self):
        restore_settings(Geometry=self.restoreGeometry,
                         Splitter=self.splitter.restoreState)

    def _saveSettings(self):
        settings = QSettings()
        settings.setValue("Geometry", self.saveGeometry())
        settings.setValue("Splitter", self.splitter.saveState())

    def _getData(self, tags):
        data = {}
        for tag in tags:
            obj = self.lineEditMap[tag]
            try:
                text = unicode(obj.text())
            except AttributeError:
                text = unicode(obj.toPlainText())
            data[tag] = text
        return data

    def _saveToSelected(self, data, action=False):
        flacs = self._selectedFlacs()
        if not action and 'title' in data and len(flacs) > 1:
            return  # don't hose a bunch of titles "accidently"
        shelf = shelve.open(SHELF)
        for flac in flacs:
            for tag in data:
                flac[tag] = data[tag]
            flac.save()
            self._update(shelf, flac, data)
        shelf.close()

    def _update(self, shelf, flac, data):
        flacdata = dict((key, flac[key][0]) for key in flac)
        filename = flac.filename #.encode('utf8')
        flacdata['filename'] = filename
        shelf[filename] = flacdata

    def _updateCompleters(self, data):
        for tag in data:
            if tag in self.tagSets:
                self.lineEditMap[tag].completer().model().refresh()

    def _selectedFlacs(self):
        flacs = []
        for index in self.dirTree.selectionModel().selectedIndexes():
            path = unicode(self.dirTree.model().filePath(index))
            if flacpath(path):
                flacs.append(FLAC(path))
        return flacs

    @Slot("QModelIndex")
    def on_dirTree_clicked(self, index):
        path = unicode(self.dirTree.model().filePath(index))
        if not flacpath(path):
            return
        audio = FLAC(path)
        rem, mins = modf(audio.info.length / 60.0)
        duration = "%d:%02d" % (mins, rem * 60)
        self.durationLabel.setText(duration)
        for tag, le in self.lineEditMap.items():
            val = audio.get(tag, [''])[0]
            le.setText(val)
        self._setWorksCompleter()

    def _populateWorks(self, items):
        workdata = {}
        for filename, data in items:
            composer = data.get('composer')
            if not composer:
                continue
            work = data.get('work')
            if not work:
                continue
            key = (composer, work, data.get('worksort', ''),
                   data.get('genre', ''))
            workdata.setdefault(key, []).append(filename)
        # sort by worksort if it exists

        def sortkey(key):
            return (key[0], key[2]) if key[2] else (key[0], key[1])

        # the "invariants" in this editable data are the filenames, at least
        # until we rescan the library, so use one of the filenames as a
        # key
        keys = sorted(workdata, key=sortkey)
        self.tableWidget.clear()
        self.tableWidget.setSortingEnabled(False)
        self.tableWidget.setRowCount(len(keys))
        headers = ['Composer', 'Work', 'WorkSort', 'Genre']
        self.tableWidget.setColumnCount(len(headers))
        self.tableWidget.setHorizontalHeaderLabels(headers)
        encoding = os.sys.getfilesystemencoding()
        self.tabledata = {}
        for row, key in enumerate(keys):
            item = None
            for i in range(len(key)):
                item = QTableWidgetItem(key[i])
                self.tableWidget.setItem(row, i, item)
            # set a key as data on the last item
            if item:
                tablekey = workdata[key][0] #.decode(encoding)
                self.tabledata[tablekey] = workdata[key]
                item.setData(Qt.UserRole, tablekey)
        self.tableWidget.resizeColumnsToContents()
        self.tableWidget.setSortingEnabled(True)

    def tableItemChanged(self, item):
        row = self.tableWidget.currentRow()
        if row < 0:
            return
        col = self.tableWidget.currentColumn()
        tag = ['composer', 'work', 'worksort', 'genre'][col]
        val = unicode(item.text()).strip()
        lastcol = self.tableWidget.columnCount() - 1
        lastitem = self.tableWidget.item(row, lastcol)
        key = unicode(lastitem.data(Qt.UserRole).toString())
        filenames = self.tabledata[key]
        for name in filenames:
            flac = FLAC(name)
            flac[tag] = val
            flac.save()


def main():
    app = QApplication(sys.argv)
    app.setOrganizationName("David Cook")
    app.setOrganizationDomain("davidcook.org")
    app.setApplicationName("QFlacTagger")

    form = Tagger()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
