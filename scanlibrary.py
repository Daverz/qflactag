from collections import defaultdict
import os

VORBIS_COMMENT_RE = '^(.*):    comment\[\d+\]: (\w+)=(.*)$'

FILE_EXTENSIONS = ('.flc', '.flac', '.fla')  # LMS allowed FLAC extensions

FLAC_SCAN = ('find {} -iname "{}" -print0 | '
             'xargs -0 metaflac '
             '--with-filename --list --block-type=VORBIS_COMMENT | '
             'gzip --stdout > {}')

VORBIS_COMMENTS_FILE = 'vorbis_comments.gz'

SINGLE_TAGS = ['composer', 'artist', 'conductor', 'band', 'genre']

SEPARATOR = ';'


def process_single_tags(tagdata):
    tagsets = defaultdict(set)
    for data in tagdata.values():
        for tag in SINGLE_TAGS:
            if tag in data and data[tag]:
                values = [s.strip() for s in data[tag].split(SEPARATOR)]
                tagsets[tag].update(values)
    return {tag: sorted(tagsets[tag]) for tag in tagsets}


def scan(root, file_extensions):
    """Dump tag data for each file into dictionary."""
    matching_filenames = []
    for root, dirs, files in os.walk(root):
        # print(root)
        for f in files:
            if f.lower().endswith(file_extensions):
                path = os.path.join(root, f)
                matching_filenames.append(path)
    return matching_filenames


def collect_tags(filenames):
    import mutagen

    errors = []
    tagdata = defaultdict(dict)
    for f in filenames:
        try:
            header = mutagen.File(f)
            tagdata[f].update(header)
        except:
            errors.append(f)
    return tagdata, errors


def flac_scan(root, glob_pattern, output=VORBIS_COMMENTS_FILE):
    import subprocess

    command = FLAC_SCAN.format(root, glob_pattern, output)
    subprocess.check_call(command, shell=True)


def parse_vorbis_comments(root, vorbis_comments_file=VORBIS_COMMENTS_FILE):
    import gzip
    import re

    vorbis_comment_re = re.compile(VORBIS_COMMENT_RE)
    path = os.path.join(root, vorbis_comments_file)
    tagdata = defaultdict(dict)
    with gzip.open(path, 'rt', encoding='utf8') as infile:
        for line in infile:
            match = vorbis_comment_re.match(line.rstrip())
            if match:
                filename, tag, value = match.groups()
                tagdata[filename][tag] = value
    return tagdata

if __name__ == '__main__':
    collect_tags(scan('.', FILE_EXTENSIONS))
    # import json
    # tags, errors = scan('.', FILE_EXTENSIONS)
    # with open('tags.json', 'w') as outfile:
    #     json.dump(tags, outfile)
    # if errors:
    #     print('There were errors reading the following files:')
    #     for e in errors:
    #         print(e)


