create table composer (
  id integer primary key,
  composer text not null unique
);

create table artist (
  id integer primary key,
  artist text not null unique,
  artistsort text
);

create table genre (
  id integer primary key,
  genre text not null unique
);

create table conductor (
  id integer primary key,
  conductor text not null unique
);

create table band (
  id integer primary key not null,
  band text not null unique
);

create table work (
  id integer primary key,
  composer_id integer references composer(id) not null,
  work text not null,
  genre_id integer references genre(id),
  worksort text,
  unique(composer_id, work)
);

/*
create table recording (
  id integer primary key,
  work_id integer references work(id) not null,
  recording_description text not null,
  unique(work_id, recording_description)
);

create table title (
  id integer primary key,
  work_id integer references work(id) not null,
  title text not null,
  title_sort integer not null, -- not the TITLESORT tag
  unique(work_id, title, title_sort)
);

create table track (
  id integer primary key,
  relative_path text not null unique -- path relative to music library root
);

create table trackcomposer (
  id integer primary key,
  track_id integer references track(id) not null,
  composer_id integer references composer(id) not null
);

create table trackwork (
  id integer primary key,
  track_id integer references track(id) not null,
  work_id integer references work(id) not null
);

create table tracktitle (
  id integer primary key,
  track_id integer references track(id) not null,
  title_id integer references title(id) not null
);

create table trackartist (
  id integer primary key,
  track_id integer references track(id) not null,
  artist_id integer references artist(id) not null
);

create table trackconductor (
  id integer primary key,
  track_id integer references track(id) not null,
  conductor_id integer references conductor(id) not null
);

create table trackband (
  id integer primary key,
  track_id integer references track(id) not null,
  band_id integer references band(id) not null
);

create table trackgenre (
  id integer primary key,
  track_id integer references track(id) not null,
  genre_id integer references genre(id) not null
);
*/
