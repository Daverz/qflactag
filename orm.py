from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy import UniqueConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.exc import NoResultFound

Base = declarative_base()

_session = None

class Composer(Base):
    __tablename__ = 'composer'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)


class Genre(Base):
    __tablename__ = 'genre'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)


class Artist(Base):
    __tablename__ = 'artist'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)


class Work(Base):
    __tablename__ = 'work'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    sort = Column(String)
    genre_id = Column(Integer, ForeignKey('genre.id'))
    composer_id = Column(Integer, ForeignKey('composer.id'), nullable=False)
    # Use cascade='delete,all' to propagate the deletion
    #  of a Department onto its Employees
    composer = relationship(
        Composer,
        backref=backref('works',
                         uselist=True,
                         cascade='delete,all'))
    genre = relationship(Genre)
    UniqueConstraint(composer_id, name, sort)


def get_class(key):
    return globals()[key.capitalize()]


def get_by_name(klass, name):
    try:
        obj = _session.query(klass).filter(getattr(klass, 'name') == name).one()
    except NoResultFound:
        obj = None
    return obj


def get_names(key, data):
    if key == 'work':
        return get_works(data['composer'])
    result = _session.query(get_class(key)).all()
    names = [o.name for o in result]
    names.sort(key=lambda s: s.lower())
    return names


def add_by_name(key, data):
    klass = get_class(key)
    name = data.get(key)
    if name:
        obj = get_by_name(klass, name)
        if not obj:
            obj = klass(name=name)
            _session.add(obj)
        return obj


def sort_key(obj):
    key = obj.sort
    if key:
        return key
    return obj.name


def get_works(composer_name):
    composer = _session.query(Composer).filter(
        Composer.name == composer_name).one()
    if composer:
        result = _session.query(Work).filter(
            Work.composer_id == composer.id).all()
        result.sort(key=sort_key)
        return [o.name for o in result]
    return []


def get_work(composer_name, work_name):
    composer = get_by_name(Composer, composer_name)
    obj = _session.query(Work).filter(Work.composer_id == composer.id and
        Work.name == work_name).one()
    return dict(work=obj.name, worksort=obj.sort, genre=obj.genre)


def init(relative_path):
    from sqlalchemy.orm import sessionmaker
    from sqlalchemy import create_engine
    global _session

    engine = create_engine('sqlite:///{}'.format(relative_path))
    maker = sessionmaker()
    maker.configure(bind=engine)
    Base.metadata.create_all(engine)
    _session = maker()


def get_session():
    global _session

    assert _session, "First use init(db_path)."
    return _session
